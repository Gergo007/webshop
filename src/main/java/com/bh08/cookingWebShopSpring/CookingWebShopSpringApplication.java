package com.bh08.cookingWebShopSpring;

import java.util.List;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbIngredientListDto;
import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbMealDto;
import com.bh08.cookingWebShopSpring.services.MealDbService;
import com.bh08.cookingWebShopSpring.services.RecipeServices;

@SpringBootApplication
public class CookingWebShopSpringApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication
				.run(new Class<?>[] { CookingWebShopSpringApplication.class, SpringConfig.class }, args);
		MealDbService serv = context.getBean(MealDbService.class);
		RecipeServices recipeServ = context.getBean(RecipeServices.class);

		boolean downloadRecipes = context.getEnvironment().getProperty("cookingwebshop.downloadRecipes", "false")
				.equalsIgnoreCase("true");

		if (downloadRecipes) {
			System.out.println("1");
			MealDbIngredientListDto listOfIngredients = serv.downloadIngredients();
			recipeServ.saveIngredients(listOfIngredients);

			System.out.println("2");
			Set<String> mealListById = serv.downloadRecipe();

			System.out.println("3");
			List<MealDbMealDto> finalMealList = serv.downloadMeals(mealListById);

			System.out.println("4");
			List<Recipe> recipes = recipeServ.createRecipes(finalMealList);

			System.out.println("5");
			recipeServ.saveRecipeToDb(recipes);
		}

		System.out.println("Done");

	}

}
