package com.bh08.cookingWebShopSpring.viewmodel;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserFormData {
	private String requestType;
	@NotEmpty
	@Min(4)
	private String userName;
	@NotEmpty
	@Min(6)
	private String password;
	@NotEmpty
	@Min(6)
	private String confirmPassword;
	
	public UserFormData(String requestType, @NotEmpty @Min(4) String userName, @NotEmpty @Min(6) String password) {
		super();
		this.requestType = requestType;
		this.userName = userName;
		this.password = password;
	}


}
