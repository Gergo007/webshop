package com.bh08.cookingWebShopSpring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.cookingWebShopSpring.daos.UserRepository;
import com.bh08.cookingWebShopSpring.models.User;

@Service
public class ProfileService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User loadUserData(String userName) {
		User user = (userRepository.findByUserName(userName)).get();
		return user;
	}
	
	public void updateUserData(String userName, String name, String phoneNumber, String address, String eMail) {
		User user = (userRepository.findByUserName(userName)).get();
		user.setName(name);
		user.setPhoneNumber(phoneNumber);
		user.setAddress(address);
		user.setEMail(eMail);
		userRepository.saveAndFlush(user);
	}

}
