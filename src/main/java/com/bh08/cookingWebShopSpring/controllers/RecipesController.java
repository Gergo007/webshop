package com.bh08.cookingWebShopSpring.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bh08.cookingWebShopSpring.models.Product;
import com.bh08.cookingWebShopSpring.models.Recipe;
import com.bh08.cookingWebShopSpring.models.User;
import com.bh08.cookingWebShopSpring.services.RecipeServices;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.services.UserService;
import com.bh08.cookingWebShopSpring.viewmodel.SearchFormData;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@Controller
public class RecipesController {
	
	@Autowired
	private RecipeServices rdbs;
	
	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/recipes", method = RequestMethod.GET)
	public String showRecipes(Model model, @RequestParam(value = "page", required = false) Integer pageNr,
			@ModelAttribute("searchFormData") SearchFormData searchFormData) {

		if (searchFormData.getSearchExpression() != null) {
			if (pageNr == null) {
				pageNr = 0;
			}

			Page<Recipe> recipes = rdbs.findByTitleIgnoreCaseContains(pageNr, searchFormData.getSearchExpression());
			model.addAttribute("recipes", recipes);
			model.addAttribute("maxPages", recipes.getTotalPages());
			model.addAttribute("userFormData", new UserFormData());
			model.addAttribute("currentPage", pageNr);
			model.addAttribute("currentUserName", sessionService.getUserName());

		} else {
			if (pageNr == null) {
				pageNr = 0;
			}

			Page<Recipe> recipes = rdbs.getAllRecipes(pageNr);
			model.addAttribute("recipes", recipes);
			model.addAttribute("maxPages", recipes.getTotalPages());
			model.addAttribute("userFormData", new UserFormData());
			model.addAttribute("currentPage", pageNr);
			model.addAttribute("currentUserName", sessionService.getUserName());
		}

		return "recipes.html";
	}

	@RequestMapping(value = "/meal", method = RequestMethod.GET)
	public String showMeal(Model model, @RequestParam(value = "id", required = false) Long recipeId) {

		User user = getCurrentUser();

		Recipe recipe = rdbs.getRecipeDetails(recipeId);
		String recipeLiked = determineRecipeLikeStatus(user, recipe);

		List<String> products = collectIngredients(recipe);

		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("recipe", recipe);
		model.addAttribute("recipeLiked", recipeLiked);
		model.addAttribute("products", products);
		model.addAttribute("currentUserName", sessionService.getUserName());

		return "meal.html";
	}

	private String determineRecipeLikeStatus(User user, Recipe recipe) {
		String recipeLiked;
		if (user == null) {
			recipeLiked = "Like";
		} else {
			if (user.getFavouriteRecipes().contains(recipe)) {
				recipeLiked = "Dislike";
			} else {
				recipeLiked = "Like";
			}
		}
		return recipeLiked;
	}

	private List<String> collectIngredients(Recipe recipe) {
		List<String> products = new ArrayList<>();
		for (Product prod : recipe.getProducts()) {
			products.add(prod.getName());
		}
		return products;
	}

	private User getCurrentUser() {
		User user = null;
		if (sessionService.getUserName() != null) {
			user = userService.findUserByName(sessionService.getUserName());
		}
		return user;
	}

}
