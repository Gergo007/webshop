package com.bh08.cookingWebShopSpring.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bh08.cookingWebShopSpring.exceptions.LoginFailedException;
import com.bh08.cookingWebShopSpring.exceptions.PwDontMatchException;
import com.bh08.cookingWebShopSpring.exceptions.PwTooShortException;
import com.bh08.cookingWebShopSpring.exceptions.UsernameIsTakenException;
import com.bh08.cookingWebShopSpring.exceptions.UsernameTooShortException;
import com.bh08.cookingWebShopSpring.models.User;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.services.UserService;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@RestController
@RequestMapping("/api")
public class UserAPIController {

	@Autowired
	private UserService userService;
	@Autowired
	private SessionService sessionService;

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public @ResponseBody User loginAndReg(@RequestBody UserFormData userFormData) throws LoginFailedException,
			UsernameIsTakenException, UsernameTooShortException, PwTooShortException, PwDontMatchException {

		if (userFormData.getRequestType().equals("login")) {

			try {

				User user = userService.login(userFormData.getUserName(), userFormData.getPassword());
				sessionService.setUserName(user.getUserName());
				return user;

			} catch (LoginFailedException e) {
				throw new LoginFailedException();
			}

		} else {

			try {

				User user = userService.registerUser(userFormData.getUserName(), userFormData.getPassword(),
						userFormData.getConfirmPassword());
				return user;
			} catch (UsernameIsTakenException e) {
				throw new UsernameIsTakenException();
			} catch (UsernameTooShortException e) {
				throw new UsernameTooShortException();
			} catch (PwDontMatchException e) {
				throw new PwDontMatchException();
			} catch (PwTooShortException e) {
				throw new PwTooShortException();
			}

		}
	}
}
