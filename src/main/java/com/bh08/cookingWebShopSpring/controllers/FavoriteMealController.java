package com.bh08.cookingWebShopSpring.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bh08.cookingWebShopSpring.models.integration.mealdb.MealDbMealDto;
import com.bh08.cookingWebShopSpring.services.RecipeServices;
import com.bh08.cookingWebShopSpring.services.SessionService;
import com.bh08.cookingWebShopSpring.viewmodel.UserFormData;

@RestController
@RequestMapping("/api")
public class FavoriteMealController {
	
	@Autowired
	private RecipeServices rdbs;

	@Autowired
	private SessionService sessionService;

	@RequestMapping(value = "/toggleFavorite", method = RequestMethod.GET)
	public MealDbMealDto toggleFav(Model model, @RequestParam("mealId") Long mealId) {
		
		rdbs.saveOrDeleteById(mealId, sessionService.getUserName());

		model.addAttribute("userFormData", new UserFormData());
		model.addAttribute("currentUserName", sessionService.getUserName());

		return new MealDbMealDto();
	}

}
