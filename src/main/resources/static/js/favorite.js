$(document).ready(function() {
	
	$('#favButton').click(function(e) {
		e.preventDefault();
		
		var button = $(this);
		var buttonText = button.val(); 
		var url_string = window.location.href;
		var url = new URL(url_string);
		var mealId = url.searchParams.get("id");
		console.log(buttonText);
		if (buttonText == "Like") {
			
			$.ajax({
				url: 'http://localhost:8092/api/toggleFavorite?mealId=' + mealId,
				method: 'GET',
				headers: {
					'Content-Type' : 'application/json',
					'Accept': 'application/json'
				},
				
				success: function(data) {
					button.val("Dislike");
					location.reload(true);
					
				},
				error: function(xhr) {
					console.error(xhr);
				}
				
			});
			
		} else {
			
			$.ajax({
				url: 'http://localhost:8092/api/toggleFavorite?mealId=' + mealId,
				method: 'GET',
				headers: {
					'Content-Type' : 'application/json',
					'Accept': 'application/json'
				},
				
				success: function(data) {
					button.val("Like");
					location.reload(true);
					
				},
				error: function(xhr) {
					console.error(xhr);
				}
			});
		}
		
		console.log(mealId);
		
	});
	
});